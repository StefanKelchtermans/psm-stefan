﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMClassLibrary
{
    public class CountryExistsException : Exception
    {
        public CountryExistsException() 
            : base()
        {
            
        }
        public CountryExistsException(String message)
            : base(message)
        {

        }
        public CountryExistsException(string format, params object[] args)
            : base(string.Format(format, args))
        {

        }
        public CountryExistsException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
        public CountryExistsException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException)
        {

        }
    }
}
