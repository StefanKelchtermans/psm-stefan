﻿//using PSMClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PSMData;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for CustomerSearchWindow.xaml
    /// </summary>
    public partial class CustomerSearchWindow : Window
    {
        public CustomerSearchTerms searchTerms = new CustomerSearchTerms();

        public CustomerSearchWindow()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void ClearForm()
        {
            codeTextBox.Text = "";
            nameTextBox.Text = "";
            addressTextBox.Text = "";
            zipcodeComboBox.SelectedIndex = -1;
            gateModelComboBox.SelectedIndex = -1;
            gateColorTextBox.Text = "";
            installationAfterDatePicker.SelectedDate = null;
            searchTerms = new CustomerSearchTerms();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            searchTerms.Code = codeTextBox.Text;
            searchTerms.Name = nameTextBox.Text;
            searchTerms.Address = addressTextBox.Text;
            searchTerms.GateColor = gateColorTextBox.Text;
            if (zipcodeComboBox.SelectedIndex > -1)
                searchTerms.City = (City)zipcodeComboBox.SelectedItem;
            if (gateModelComboBox.SelectedIndex > -1)
                searchTerms.GateModel = (GateModel)gateModelComboBox.SelectedItem;
            if (installationAfterDatePicker.SelectedDate != null)
                searchTerms.InstalationAfter = (DateTime)installationAfterDatePicker.SelectedDate;
            DialogResult = true;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            City_services cService = new City_services();
            List<City> cities = cService.GetAll();
            zipcodeComboBox.ItemsSource = cities;
            zipcodeComboBox.DisplayMemberPath = "FullCity";

            Gatemodel_services gmService = new Gatemodel_services();
            List<GateModel> gatemodels = gmService.GetAll();
            gateModelComboBox.ItemsSource = gatemodels;
            gateModelComboBox.DisplayMemberPath = "Model";

            codeTextBox.Focus();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                Search();
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

    }
}
