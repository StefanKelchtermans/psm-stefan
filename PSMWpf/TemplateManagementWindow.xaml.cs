﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for TemplateManagementWindow.xaml
    /// </summary>
    public partial class TemplateManagementWindow : Window
    {
        private PSMUser currentUser;
        private ObservableCollection<Template> templates;
        private System.Windows.Data.CollectionViewSource templateViewSource;

        public TemplateManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (templateDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u het sjabloontype verwijderen?", "Verwijderen sjabloontype", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Template template = (Template)templateDataGrid.SelectedItem;
                    Template_service tService = new Template_service();
                    template.Modified = DateTime.Now;
                    template.ModifiedBy = currentUser.FirstName;
                    tService.Delete(template);
                    ReloadTemplates();
                    MessageBox.Show("sjabloontype verwijderd.", "Verwijderen sjabloontype");
                }
            }

        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<Template>(from m in templates
                                                                   where m.TemplateType.ToLower().StartsWith(searchString.ToLower())
                                                                  orderby m.TemplateType
                                                                   select m).ToList();

                templateViewSource.Source = gevonden;
            }
            else
                templateViewSource.Source = templates;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            templateViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("templateViewSource")));
            ReloadTemplates();
        }

        private void ReloadTemplates()
        {
            Template_service tService = new Template_service();
            templates = tService.GetAllObservable();
            templateViewSource.Source = templates;
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (templateDataGrid.SelectedIndex > -1)
            {
                Template template = (Template)templateDataGrid.SelectedItem;
                if (template.Id == 0)
                {
                    int i = 0;
                    foreach (Template item in templates)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    templates.RemoveAt(i);
                    templateViewSource.Source = null;
                    templateViewSource.Source = templates;
                }
            }
        }


        private void NewElement()
        {
            Template newTemplate = new Template();
            newTemplate.Id = 0;
            templates.Add(newTemplate);
            templateViewSource.Source = null;
            templateViewSource.Source = templates;
            templateDataGrid.SelectedIndex = templates.Count - 1;
            templateDataGrid.ScrollIntoView(newTemplate);
        }

        private void SaveData()
        {
            if (templateDataGrid.SelectedIndex > -1)
            {
                Template template = (Template)templateDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateTemplate(template);
                if (valid.IsValid)
                {
                    Template_service tService = new Template_service();
                    if (template.Id == 0)
                    {
                        template.Active = true;
                        template.Created = DateTime.Now;
                        template.CreatedBy = currentUser.FirstName;
                        template.Modified = DateTime.Now;
                        template.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            Template saved = tService.Save(template);
                            ReloadTemplates();
                            MessageBox.Show("Sjabloontype opgeslagen.", "Opslaan Sjabloontype");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        template.Modified = DateTime.Now;
                        template.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            tService.Update(template);
                            ReloadTemplates();
                            MessageBox.Show("Sjabloontype opgeslagen.", "Opslaan Sjabloontype");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
