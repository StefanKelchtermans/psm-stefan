﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PSMData;
using PSMClassLibrary;
using System.Collections.ObjectModel;
using ITCLibrary;
using Microsoft.Win32;
using Xceed.Wpf.Toolkit;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Windows.Data.CollectionViewSource customerViewSource;
        private System.Windows.Data.CollectionViewSource customerGateViewSource;
        private System.Windows.Data.CollectionViewSource interventionViewSource;
        private ObservableCollection<Customer> currentCustomers;
        private ObservableCollection<CustomerGate> currentGates;
        private ObservableCollection<Intervention> currentInterventions;
        private Customer currentCustomer;
        private CustomerGate currentGate;
        private bool IsChanged = false;
        private int CustomerTabIndex = 0;
        public ObservableCollection<PSMCheckBoxListBoxItem> AvailableSafetyMeasures;
        public ObservableCollection<PSMCheckBoxListBoxItem> AvailableInstallations;
        private PSMUser currentUser;

        public MainWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            LoadCityComboBox();
            LoadLegalCapacityComboBox();
            LoadLanguageComboBox();
            LoadGateModelComboBox();
            LoadRailsystemComboBox();
            LoadGateDriverComboBox();
            LoadRemoteControlComboBox();
            LoadEngineTypeComboBox();
            LoadMaintenanceFrequencyComboBox();
            LoadGateDecorationComboBox();
            LoadVoltageComboBox();
            LoadInterventionTypeComboBox();

            SetSafetyMeasuresListBox();
            SetServiceInstalationListBox();

            interventionViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("interventionViewSource")));
            customerGateViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("customerGateViewSource")));
            customerViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("customerViewSource")));
            Customer_services cService = new Customer_services();
            ObservableCollection<Customer> customers = cService.GetAll();
            currentCustomers = customers;
            customerViewSource.Source = customers;

            SetButtons();
        }

        private void SetButtons()
        {
            if (IsChanged)
            {
                SaveButton.IsEnabled = true;
                NewButton.IsEnabled = false;
                SearchCustomerButton.IsEnabled = false;
                DeleteButton.IsEnabled = false;
            }
            else
            {
                SaveButton.IsEnabled = false;
                NewButton.IsEnabled = true;
                SearchCustomerButton.IsEnabled = true;
                DeleteButton.IsEnabled = true;
            }
        }

        private void SetServiceInstalationListBox()
        {
            Service_Installation_services siService = new Service_Installation_services();
            List<ServiceInstallation> instalations = siService.GetAll();
            AvailableInstallations = new ObservableCollection<PSMCheckBoxListBoxItem>();
            foreach (ServiceInstallation item in instalations)
            {
                PSMCheckBoxListBoxItem service = new PSMCheckBoxListBoxItem();
                service.id = item.Id;
                service.Name = item.Name;
                service.IsChecked = false;
                AvailableInstallations.Add(service);
            }
            serviceInstallationListBox.ItemsSource = AvailableInstallations;
        }

        private void SetSafetyMeasuresListBox()
        {
            SafetyMeasure_services smService = new SafetyMeasure_services();
            List<SafetyMeasure> safetyMeasures = smService.GetAll();
            AvailableSafetyMeasures = new ObservableCollection<PSMCheckBoxListBoxItem>();
            foreach (SafetyMeasure item in safetyMeasures)
            {
                PSMCheckBoxListBoxItem measure = new PSMCheckBoxListBoxItem();
                measure.id = item.Id;
                measure.Name = item.Name;
                measure.IsChecked = false;
                AvailableSafetyMeasures.Add(measure);
            }
            safetyMeasureListBox.ItemsSource = AvailableSafetyMeasures;
        }

        private void customerDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid DG = (DataGrid)sender;
            if (DG.Items.Count > 0)
            {
                Customer customer = (Customer)DG.SelectedItem;
                currentCustomer = customer;
                if (customer != null)
                {
                    //Set city to combobox
                    if (customer.City1 != null)
                        zipcodeComboBox.Text = customer.City1.FullCity;
                    else
                        zipcodeComboBox.SelectedIndex = -1;
                    //Set Legalcapacity to comboBox
                    if (customer.LegalCapacity1 != null)
                        legalCapacityComboBox.Text = customer.LegalCapacity1.Name;
                    else
                        legalCapacityComboBox.SelectedIndex = -1;
                    //Set Language to combobox
                    if (customer.Language1 != null)
                        languageComboBox.Text = customer.Language1.Name;
                    else
                        languageComboBox.SelectedIndex = -1;
                }

                SetCustomerGates();
            }
            else
            {
                ClearCustomerForm();
                currentCustomer = null;
                ClearCustomerGateList();
            }
        }

        private void ClearCustomerGateList()
        {
            customerGateViewSource.Source = new ObservableCollection<CustomerGate>();
            ClearCustomerGateForm();
            ClearInterventionList();
        }

        private void ClearInterventionList()
        {
            interventionViewSource.Source = new ObservableCollection<Intervention>();
            ClearInterventionForm();
        }

        private void ClearInterventionForm()
        {
            interventionTypeComboBox.SelectedIndex = -1;
            waitForFeedBackComboBox.SelectedIndex = -1;
        }

        private void SetCustomerGates()
        {
            try
            {
                if (currentCustomer != null)
                {
                    Customer_Gate_services cgService = new Customer_Gate_services();
                    ObservableCollection<CustomerGate> customerGates = cgService.GetByCustomer(currentCustomer.Id);
                    customerGateViewSource.Source = null;
                    customerGateViewSource.Source = customerGates;
                    currentGates = customerGates;
                    if (customerGates.Count == 0)
                        ClearCustomerGateForm();
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void ClearCustomerForm()
        {
            zipcodeComboBox.SelectedIndex = -1;
            legalCapacityComboBox.SelectedIndex = -1;
            languageComboBox.SelectedIndex = -1;
        }



        private void zipcodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Customer customer = (Customer)customerDataGrid.SelectedItem;
                City city = (City)box.SelectedItem;
                customer.City = city.Id;
                customer.City1 = city;
            }
        }

        private void legalCapacityComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Customer customer = (Customer)customerDataGrid.SelectedItem;
                LegalCapacity capacity = (LegalCapacity)box.SelectedItem;
                customer.LegalCapacity = capacity.Id;
                customer.LegalCapacity1 = capacity;
            }
        }

        private void languageComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            if (box.SelectedIndex > -1)
            {
                Customer customer = (Customer)customerDataGrid.SelectedItem;
                Language language = (Language)box.SelectedItem;
                customer.Language = language.Id;
                customer.Language1 = language;
            }
        }


        private void SearchCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            OpenSearchWindow();
        }

        private void OpenSearchWindow()
        {
            CustomerSearchWindow SW = new CustomerSearchWindow();
            if (SW.ShowDialog() == true)
            {
                CustomerSearchTerms searchterms = SW.searchTerms;

                FilterCustomers(searchterms);
            }
        }

        private void FilterCustomers(CustomerSearchTerms searchterms)
        {
            Customer_services cService = new Customer_services();
            ObservableCollection<Customer> customers = cService.GetBySearchTerms(searchterms);
            customerViewSource.Source = customers;
            currentCustomers = customers;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void NewElement()
        {
            TabItem tab = (TabItem)CustomerTabControl.SelectedItem;
            switch (tab.Header.ToString())
            {
                case "Klant":
                    CreateNewCustomer();
                    break;
                case "Poort":
                    CreateNewCustomerGate();
                    break;
                case "Interventie":
                    CreateNewIntervention();
                    break;
                default:
                    break;
            }

        }

        private void CreateNewIntervention()
        {
            Intervention newIntervention = new Intervention();
            newIntervention.Id = 0;
            newIntervention.CustomerGate = currentGate.Id;
            currentInterventions.Add(newIntervention);
            interventionViewSource.Source = null;
            interventionViewSource.Source = currentInterventions;
            interventionDataGrid.SelectedIndex = currentInterventions.Count - 1;
            interventionDataGrid.ScrollIntoView(newIntervention);

            IsChanged = true;
            SetButtons();
        }

        private void CreateNewCustomerGate()
        {
            CustomerGate newGate = new CustomerGate();
            newGate.Id = 0;
            newGate.Customer = currentCustomer.Id;
            currentGates.Add(newGate);
            customerGateViewSource.Source = null;
            customerGateViewSource.Source = currentGates;
            ClearCustomerGateForm();
            customerGateDataGrid.SelectedIndex = currentGates.Count - 1;
            customerGateDataGrid.ScrollIntoView(newGate);

            IsChanged = true;
            SetButtons();
        }

        private void ClearCustomerGateForm()
        {
            gateModelComboBox.SelectedIndex = -1;
            railSystemComboBox.SelectedIndex = -1;
            gateDriverComboBox.SelectedIndex = -1;
            maintenanceContractComboBox.SelectedIndex = -1;
            remoteControlComboBox.SelectedIndex = -1;
            engineTypeComboBox.SelectedIndex = -1;
            maintenanceFrequencyComboBox.SelectedIndex = -1;
            gateDecorationComboBox.SelectedIndex = -1;
            voltageComboBox.SelectedIndex = -1;
        }

        private void CreateNewCustomer()
        {
            Customer newCustomer = new Customer();
            newCustomer.Id = 0;
            currentCustomers.Add(newCustomer);
            customerViewSource.Source = null;
            customerViewSource.Source = currentCustomers;
            customerDataGrid.SelectedIndex = currentCustomers.Count - 1;
            customerDataGrid.ScrollIntoView(newCustomer);

            IsChanged = true;
            SetButtons();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            TabItem tab = (TabItem)CustomerTabControl.SelectedItem;
            switch (tab.Header.ToString())
            {
                case "Klant":
                    SaveCustomer();
                    break;
                case "Poort":
                    SaveCustomerGate();
                    break;
                case "Interventie":
                    SaveIntervention();
                    break;
                default:
                    break;
            }
        }

        private void SaveIntervention()
        {
            Intervention intervention = (Intervention)interventionDataGrid.SelectedItem;
            if (intervention != null)
            {
                if (intervention.Id == 0)
                {
                    intervention.InterventionType1 = null;
                    intervention.Active = true;
                    intervention.Created = DateTime.Now;
                    intervention.Modified = intervention.Created;
                    intervention.CreatedBy = currentUser.FirstName;
                    intervention.ModifiedBy = currentUser.FirstName;
                    Valid valid = OurValidation.ValidateIntervention(intervention);
                    if (valid.IsValid)
                    {
                        try
                        {
                            Intervention_services iService = new Intervention_services();
                            Intervention savedIntervention = iService.Save(intervention);
                            GetInterventions(currentGate.Id);
                            System.Windows.MessageBox.Show("Interventie is opgeslagen.", "Klant");

                            IsChanged = false;
                            SetButtons();
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(valid.Message);
                    }
                }
                else
                {
                    intervention.Modified = intervention.Created;
                    intervention.ModifiedBy = currentUser.FirstName;
                    Valid valid = OurValidation.ValidateIntervention(intervention);
                    if (valid.IsValid)
                    {
                        try
                        {
                            Intervention_services iService = new Intervention_services();
                            Intervention savedIntervention = iService.Update(intervention);
                            GetInterventions(currentGate.Id);
                            System.Windows.MessageBox.Show("Interventie is opgeslagen.", "Klant");

                            IsChanged = false;
                            SetButtons();
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(valid.Message);
                    }
                }
            }
        }

        private void SaveCustomerGate()
        {
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                if (gate.Id == 0)
                {
                    gate.GateModel1 = null;
                    gate.RailSystem1 = null;
                    gate.GateDriver1 = null;
                    gate.MaintenanceFrequency1 = null;
                    gate.RemoteControl1 = null;
                    gate.EngineType1 = null;
                    gate.GateDecoration1 = null;
                    gate.Voltage1 = null;
                    gate.Active = true;
                    gate.Created = DateTime.Now;
                    gate.CreatedBy = currentUser.FirstName;
                    gate.Modified = gate.Created;
                    gate.ModifiedBy = currentUser.FirstName;
                    Valid valid = OurValidation.ValidateCustomerGate(gate);
                    if (valid.IsValid)
                    {
                        try
                        {
                            Customer_Gate_services cgService = new Customer_Gate_services();
                            CustomerGate savedGate = cgService.Save(gate);

                            SaveSafetyMaesures(gate);
                            SaveServiceInstallation(gate);
                            SetCustomerGates();
                            System.Windows.MessageBox.Show("Poort is opgeslagen.", "Klant");

                            IsChanged = false;
                            SetButtons();
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(valid.Message);
                    }
                }
                else
                {
                    gate.Modified = gate.Created;
                    gate.ModifiedBy = currentUser.FirstName;
                    Valid valid = OurValidation.ValidateCustomerGate(gate);
                    if (valid.IsValid)
                    {
                        try
                        {
                            Customer_Gate_services cgService = new Customer_Gate_services();
                            cgService.Update(gate);
                            SaveSafetyMaesures(gate);
                            SaveServiceInstallation(gate);
                            SetCustomerGates();
                            System.Windows.MessageBox.Show("Poort is opgeslagen.", "Klant");

                            IsChanged = false;
                            SetButtons();
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(valid.Message);
                    }

                }
            }
        }

        private void SaveServiceInstallation(CustomerGate gate)
        {
            List<CustomerGate_ServiceInstallation> activeServices = new List<CustomerGate_ServiceInstallation>();
            List<CustomerGate_ServiceInstallation> inActiveServices = new List<CustomerGate_ServiceInstallation>();
            foreach (PSMCheckBoxListBoxItem item in serviceInstallationListBox.Items)
            {
                if (item.IsChecked)
                {
                    Service_Installation_services siService = new Service_Installation_services();
                    ServiceInstallation s = siService.GetByName(item.Name);
                    CustomerGate_ServiceInstallation installation = new CustomerGate_ServiceInstallation();
                    installation.CustomerGate = gate.Id;
                    if (s != null)
                    {
                        installation.ServiceInstallation = s.Id;
                        installation.ModifiedBy = currentUser.FirstName;
                        activeServices.Add(installation);
                    }
                }
                else
                {
                    Service_Installation_services siService = new Service_Installation_services();
                    ServiceInstallation s = siService.GetByName(item.Name);
                    CustomerGate_ServiceInstallation installation = new CustomerGate_ServiceInstallation();
                    installation.CustomerGate = gate.Id;
                    if (s != null)
                    {
                        installation.ServiceInstallation = s.Id;
                        installation.ModifiedBy = currentUser.FirstName;
                        inActiveServices.Add(installation);
                    }
                }
            }

            CustomerGate_ServiceInstallation_services cgsiService = new CustomerGate_ServiceInstallation_services();
            cgsiService.SaveList(activeServices);
            cgsiService.DeleteList(inActiveServices);
        }

        private void SaveSafetyMaesures(CustomerGate gate)
        {
            List<CustomerGate_SafetyMeasure> safetymaesures = new List<CustomerGate_SafetyMeasure>();
            List<CustomerGate_SafetyMeasure> InactiveSafetyMaesures = new List<CustomerGate_SafetyMeasure>();
            foreach (PSMCheckBoxListBoxItem item in safetyMeasureListBox.Items)
            {
                if (item.IsChecked)
                {
                    SafetyMeasure_services smService = new SafetyMeasure_services();
                    SafetyMeasure m = smService.GetByName(item.Name);
                    CustomerGate_SafetyMeasure maesure = new CustomerGate_SafetyMeasure();
                    maesure.CustomerGate = gate.Id;
                    if (m != null)
                    {
                        maesure.SafetyMeasure = m.Id;
                        maesure.ModifiedBy = currentUser.FirstName;
                        safetymaesures.Add(maesure);
                    }
                }
                else
                {
                    SafetyMeasure_services smService = new SafetyMeasure_services();
                    SafetyMeasure m = smService.GetByName(item.Name);
                    CustomerGate_SafetyMeasure maesure = new CustomerGate_SafetyMeasure();
                    maesure.CustomerGate = gate.Id;
                    if (m != null)
                    {
                        maesure.SafetyMeasure = m.Id;
                        maesure.ModifiedBy = currentUser.FirstName;
                        InactiveSafetyMaesures.Add(maesure);
                    }
                }

            }

            CustomerGate_SafetyMeasure_Services cgsmService = new CustomerGate_SafetyMeasure_Services();
            cgsmService.SaveList(safetymaesures);
            cgsmService.DeleteList(InactiveSafetyMaesures);

        }

        private void SaveCustomer()
        {
            Customer customer = (Customer)customerDataGrid.SelectedItem;
            if (customer != null)
            {
                if (customer.Id == 0)
                {
                    customer.Active = true;
                    customer.Created = DateTime.Now;
                    customer.CreatedBy = currentUser.FirstName;
                    customer.ModifiedBy = currentUser.FirstName;
                    customer.Modified = customer.Created;
                    Valid valid = OurValidation.ValidateCustomer(customer);
                    if (valid.IsValid)
                    {
                        try
                        {
                            Customer_services custService = new Customer_services();
                            Customer savedCustomer = custService.Save(customer);
                            customer.Id = savedCustomer.Id;
                            customer.City1 = savedCustomer.City1;
                            customer.LegalCapacity1 = savedCustomer.LegalCapacity1;
                            customer.Language1 = savedCustomer.Language1;
                            currentCustomer = customer;
                            System.Windows.MessageBox.Show("Klant is opgeslagen.", "Klant");
                            IsChanged = false;
                            SetButtons();
                            CustomerSearchTerms terms = new CustomerSearchTerms();
                            terms.Code = customer.Code;
                            currentCustomers = custService.GetBySearchTerms(terms);
                            customerViewSource.Source = currentCustomers;
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show(ex.Message);
                        }
                    }
                    else
                        System.Windows.MessageBox.Show(valid.Message);
                }
                else
                {
                    customer.Modified = DateTime.Now;
                    customer.ModifiedBy = currentUser.FirstName;
                    Valid valid = OurValidation.ValidateCustomer(customer);
                    if (valid.IsValid)
                    {
                        try
                        {
                            Customer_services custService = new Customer_services();
                            custService.Update(customer);
                            System.Windows.MessageBox.Show("Klant is opgeslagen.", "Klant");

                            IsChanged = false;
                            SetButtons();
                        }
                        catch (Exception ex)
                        {
                            System.Windows.MessageBox.Show(ex.Message);
                        }
                    }
                    else
                        System.Windows.MessageBox.Show(valid.Message);
                }
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            TabItem tab = (TabItem)CustomerTabControl.SelectedItem;
            switch (tab.Header.ToString())
            {
                case "Klant":
                    DeleteCustomer();
                    break;
                case "Poort":
                    DeleteCustomerGate();
                    break;
                case "Interventie":
                    DeleteIntervention();
                    break;
                default:
                    break;
            }
        }

        private void DeleteIntervention()
        {
            if (System.Windows.MessageBox.Show("Wil je deze interventie verwijderen?", "Verwijderen interventie", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                try
                {
                    Intervention intervention = (Intervention)interventionDataGrid.SelectedItem;
                    Intervention_services iService = new Intervention_services();
                    intervention.ModifiedBy = currentUser.FirstName;
                    intervention.Modified = DateTime.Now;
                    iService.Delete(intervention);
                    System.Windows.MessageBox.Show("Interventie verwijderd.", "Verwijderen interventie");
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Kan de interventie niet verwijderen!\n" + ex.Message, "Verwijderen interventie");
                }
            }
        }

        private void DeleteCustomerGate()
        {
            if (System.Windows.MessageBox.Show("Wil je deze poort verwijderen?", "Verwijderen poort", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                try
                {
                    CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
                    gate.Modified = DateTime.Now;
                    gate.ModifiedBy = currentUser.FirstName;
                    Customer_Gate_services gService = new Customer_Gate_services();
                    gService.Delete(gate);
                    System.Windows.MessageBox.Show("Poort verwijderd.", "Verwijderen poort");
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Kan de poort niet verwijderen!\n" + ex.Message, "Verwijderen poort");
                }
            }
        }

        private void DeleteCustomer()
        {
            if (System.Windows.MessageBox.Show("Wil je deze klant verwijderen?", "Verwijderen klant", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                try
                {
                    Customer customer = (Customer)customerDataGrid.SelectedItem;
                    Customer_services custService = new Customer_services();
                    customer.Modified = DateTime.Now;
                    customer.ModifiedBy = currentUser.FirstName;
                    custService.Delete(customer);
                    System.Windows.MessageBox.Show("Klant verwijderd.", "Verwijderen klant");
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Kan de klant niet verwijderen!\n" + ex.Message, "Verwijderen klant");
                }
            }
        }


        #region CustomerGate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void customerGateDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            if (dg.SelectedIndex > -1)
            {
                Customer_Gate_services cService = new Customer_Gate_services();
                CustomerGate selectedGate = (CustomerGate)dg.SelectedItem;
                if (selectedGate != null)
                {
                    CustomerGate gate = cService.GetById(selectedGate.Id);
                    currentGate = gate;
                    if (gate != null)
                    {
                        try
                        {
                            if (gate.GateModel1 != null)
                                gateModelComboBox.Text = gate.GateModel1.Model;
                            else
                                gateModelComboBox.SelectedIndex = -1;

                        }
                        catch (Exception)
                        {
                            gateModelComboBox.SelectedIndex = -1;
                        }
                        try
                        {
                            if (gate.RailSystem1 != null)
                                railSystemComboBox.Text = gate.RailSystem1.Name;
                            else
                                railSystemComboBox.SelectedIndex = -1;
                        }
                        catch (Exception)
                        {
                            railSystemComboBox.SelectedIndex = -1;
                        }
                        try
                        {
                            if (gate.GateDriver1 != null)
                                gateDriverComboBox.Text = gate.GateDriver1.Name;
                            else
                                gateDriverComboBox.SelectedIndex = -1;

                        }
                        catch (Exception)
                        {
                            gateDriverComboBox.SelectedIndex = -1;
                        }
                        try
                        {
                            if (gate.RemoteControl1 != null)
                                remoteControlComboBox.Text = gate.RemoteControl1.Name;
                            else
                                remoteControlComboBox.SelectedIndex = -1;
                        }
                        catch (Exception)
                        {
                            remoteControlComboBox.SelectedIndex = -1;
                        }
                        maintenanceContractComboBox.Text = gate.MaintenanceContractNL;
                        try
                        {
                            if (gate.EngineType1 != null)
                                engineTypeComboBox.Text = gate.EngineType1.Name;
                            else
                                engineTypeComboBox.SelectedIndex = -1;

                        }
                        catch (Exception)
                        {
                            engineTypeComboBox.SelectedIndex = -1;
                        }
                        try
                        {
                            if (gate.MaintenanceFrequency1 != null)
                                maintenanceFrequencyComboBox.Text = gate.MaintenanceFrequency1.Frequency.ToString();
                            else
                                maintenanceFrequencyComboBox.SelectedIndex = -1;
                        }
                        catch (Exception)
                        {
                            maintenanceFrequencyComboBox.SelectedIndex = -1;
                        }
                        try
                        {
                            if (gate.GateDecoration1 != null)
                                gateDecorationComboBox.Text = gate.GateDecoration1.Name;
                            else
                                gateDecorationComboBox.SelectedIndex = -1;
                        }
                        catch (Exception)
                        {
                            gateDecorationComboBox.SelectedIndex = -1;
                        }
                        try
                        {
                            if (gate.Voltage1 != null)
                                voltageComboBox.Text = gate.Voltage1.Amount.ToString();
                            else
                                voltageComboBox.SelectedIndex = -1;
                        }
                        catch (Exception)
                        {
                            voltageComboBox.SelectedIndex = -1;
                        }

                        GetCustomerGateSafetyMeasures(gate.Id);
                        GetCustomerGateServiceInstalation(gate.Id);
                        GetInterventions(gate.Id);
                    }
                    else
                        ClearCustomerGateForm();
                }
            }
        }

        private void GetInterventions(int customergate_id)
        {
            Intervention_services iService = new Intervention_services();
            ObservableCollection<Intervention> interventions = iService.GetByCustomerGate(customergate_id);
            interventionViewSource.Source = null;
            interventionViewSource.Source = interventions;
            currentInterventions = interventions;
        }

        private void GetCustomerGateServiceInstalation(int customergate_id)
        {
            foreach (PSMCheckBoxListBoxItem check in AvailableInstallations)
            {
                check.IsChecked = false;
            }

            CustomerGate_ServiceInstallation_services csService = new CustomerGate_ServiceInstallation_services();
            List<CustomerGate_ServiceInstallation> seviceinstallations = csService.GetByCustomerGate(customergate_id);
            foreach (PSMCheckBoxListBoxItem item in AvailableInstallations)
            {
                //item.IsChecked = false;
                foreach (CustomerGate_ServiceInstallation service in seviceinstallations)
                {
                    if (service.ServiceInstallation == item.id)
                    {
                        item.IsChecked = true;
                        break;
                    }
                }
            }
            serviceInstallationListBox.ItemsSource = null;
            serviceInstallationListBox.ItemsSource = AvailableInstallations;

        }

        private void GetCustomerGateSafetyMeasures(int customergate_id)
        {
            foreach (PSMCheckBoxListBoxItem check in AvailableSafetyMeasures)
            {
                check.IsChecked = false;
            }
            //safetyMeasureListBox.ItemsSource = AvailableSafetyMeasures;

            CustomerGate_SafetyMeasure_Services cgService = new CustomerGate_SafetyMeasure_Services();
            List<CustomerGate_SafetyMeasure> safetyMeasures = cgService.GetByCustomerGate(customergate_id);
            foreach (PSMCheckBoxListBoxItem item in AvailableSafetyMeasures)
            {
                //item.IsChecked = false;
                foreach (CustomerGate_SafetyMeasure measure in safetyMeasures)
                {
                    if (measure.SafetyMeasure == item.id)
                    {
                        item.IsChecked = true;
                        break;
                    }
                }
            }
            safetyMeasureListBox.ItemsSource = null;
            safetyMeasureListBox.ItemsSource = AvailableSafetyMeasures;
        }

        private void gateModelComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                GateModel model = (GateModel)box.SelectedItem;
                if (model != null)
                {
                    gate.GateModel1 = model;
                    gate.GateModel = model.Id;
                }
            }
        }

        private void railSystemComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                RailSystem rail = (RailSystem)box.SelectedItem;
                if (rail != null)
                {
                    gate.RailSystem1 = rail;
                    gate.RailSystem = rail.Id;
                }
            }
        }

        private void gateDriverComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                GateDriver driver = (GateDriver)box.SelectedItem;
                if (driver != null)
                {
                    gate.GateDriver1 = driver;
                    gate.GateDriver = driver.Id;
                }
            }
        }

        private void maintenanceContractComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                if (box.SelectedIndex > -1)
                {
                    string onderhoud = ((ComboBoxItem)box.SelectedItem).Content.ToString(); ;
                    if (onderhoud == "Ja")
                        gate.MaintenanceContract = true;
                    else if (onderhoud == "Nee")
                        gate.MaintenanceContract = false;
                }
            }
        }

        private void remoteControlComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                RemoteControl control = (RemoteControl)box.SelectedItem;
                if (control != null)
                {
                    gate.RemoteControl = control.Id;
                    gate.RemoteControl1 = control;
                }
            }
        }

        private void engineTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                EngineType enginetype = (EngineType)box.SelectedItem;
                if (enginetype != null)
                {
                    gate.EngineType1 = enginetype;
                    gate.EngineType = enginetype.Id;
                }
            }
        }

        private void maintenanceFrequencyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                MaintenanceFrequency frequentie = (MaintenanceFrequency)box.SelectedItem;
                if (frequentie != null)
                {
                    gate.MaintenanceFrequency1 = frequentie;
                    gate.MaintenanceFrequency = frequentie.Id;
                }
            }
        }

        private void gateDecorationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                GateDecoration decoration = (GateDecoration)box.SelectedItem;
                if (decoration != null)
                {
                    gate.GateDecoration1 = decoration;
                    gate.GateDecoration = decoration.Id;
                }
            }
        }

        private void voltageComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
            if (gate != null)
            {
                Voltage voltage = (Voltage)box.SelectedItem;
                if (voltage != null)
                {
                    gate.Voltage1 = voltage;
                    gate.Voltage = voltage.Id;
                }
            }
        }


        #endregion

        #region Interventions

        private void interventionDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Intervention intervention = (Intervention)dg.SelectedItem;
            if (intervention != null)
            {
                waitForFeedBackComboBox.Text = intervention.WaitForFeedBackNL;

                try
                {
                    if (intervention.InterventionType1 != null)
                        interventionTypeComboBox.Text = intervention.InterventionType1.Name;
                    else
                        interventionTypeComboBox.SelectedIndex = -1;

                }
                catch (Exception)
                {
                    interventionTypeComboBox.SelectedIndex = -1;
                }
            }
        }

        private void interventionTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            Intervention intervention = (Intervention)interventionDataGrid.SelectedItem;
            if (intervention != null)
            {
                InterventionType type = (InterventionType)box.SelectedItem;
                if (type != null)
                {
                    intervention.InterventionType = type.Id;
                    intervention.InterventionType1 = type;
                }
            }
        }

        private void waitForFeedBackComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            Intervention intervention = (Intervention)interventionDataGrid.SelectedItem;
            if (intervention != null)
            {
                string waarde = ((ComboBoxItem)box.SelectedItem).Content.ToString();
                if (waarde == "Ja")
                    intervention.WaitForFeedBack = true;
                else
                    intervention.WaitForFeedBack = false;
            }
        }

        #endregion

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (System.Windows.MessageBox.Show("Wil je het programma afsluiten?", "Afsluiten ?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                Application.Current.Shutdown();
            else
                e.Cancel = true;
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            IsChanged = true;
            SetButtons();
        }

        private void Dropdown_Closed(object sender, EventArgs e)
        {
            IsChanged = true;
            SetButtons();
        }

        private void DatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            IsChanged = true;
            SetButtons();
        }

        private void CustomerTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.OriginalSource == CustomerTabControl)
            {

                TabControl control = (TabControl)sender;
                if (IsChanged)
                {
                    e.Handled = true;
                    control.SelectedIndex = CustomerTabIndex;
                    System.Windows.MessageBox.Show("Er zijn nog onopgeslagen items in de huidige tab! sla deze eerst op.", "Wijzigen tab");
                }
                else
                    CustomerTabIndex = control.SelectedIndex;
            }
        }

        private void PrintCustomersButton_Click(object sender, RoutedEventArgs e)
        {
            PrintCustomers();
        }

        private void PrintCustomers()
        {
            try
            {
                DateTime today = DateTime.Today;
                string titel = "Klantenlijst " + today.Day + "-" + today.Month + "-" + today.Year;
                WordDocument doc = new WordDocument(titel, "Staand");
                string newLine = Environment.NewLine;
                doc.addLine("");
                doc.toggleBold();
                doc.addLine("Resultaten:");
                doc.toggleBold();
                doc.addLine("");
                foreach (Customer customer in currentCustomers)
                {
                    string zip = "";
                    string city = "";
                    if (customer.City1 != null)
                    {
                        zip = customer.City1.Zipcode;
                        city = customer.City1.Name.ToUpper();
                    }
                    //doc.addLine("");
                    doc.addLine(customer.Name + "\v" + customer.Address + ", " + zip + " " + city + "\v");
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Fout bij het genereren van het Worddocument! \n" + ex.Message);
            }
        }

        private void TemplateMenuItem_Click(object sender, RoutedEventArgs e)
        {
            TemplateManagementWindow TW = new TemplateManagementWindow(currentUser);
            if (TW.ShowDialog() == true)
            {

            }
        }

        private void TemplateDocumentMenuitem_Click(object sender, RoutedEventArgs e)
        {
            TemplateDocumentManagementWindow TDW = new TemplateDocumentManagementWindow(currentUser);
            if (TDW.ShowDialog() == true)
            {
                
            }
        }

        private void DocumentenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DocumentManagementWindow DW = new DocumentManagementWindow(currentUser);
            if (DW.ShowDialog() == true)
            {
                
            }
        }

        private void DocumentcreationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DocumentCreationWindow CW = new DocumentCreationWindow(currentUser);
            if (CW.ShowDialog() == true)
            {
                
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void MaximizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Maximized)
            {
                WindowState = System.Windows.WindowState.Normal;
                maximizeImage.Source = new BitmapImage(new Uri("pack://application:,,,/PSMWpf;component/Images/maximizeIcon.png"));
            }
            else
            {
                WindowState = System.Windows.WindowState.Maximized;
                maximizeImage.Source = new BitmapImage(new Uri("pack://application:,,,/PSMWpf;component/Images/normalIcon.png"));
            }
        }

        private void MinimizeButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                OpenSearchWindow();
            }
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.P && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                PrintCustomers();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                this.Close();
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyRow();
            }
        }

        private void RemoveEmptyRow()
        {
            TabItem tab = (TabItem)CustomerTabControl.SelectedItem;
            switch (tab.Header.ToString())
            {
                case "Klant":
                    RemoveEmptyCustomer();
                    break;
                case "Poort":
                    RemoveEmptyCustomerGate();
                    break;
                case "Interventie":
                    RemoveEmptyIntervention();
                    break;
                default:
                    break;
            }
        }

        private void RemoveEmptyIntervention()
        {
            if (interventionDataGrid.SelectedIndex > -1)
            {
                Intervention intervention = (Intervention)interventionDataGrid.SelectedItem;
                if (intervention.Id == 0)
                {
                    int index = interventionDataGrid.SelectedIndex;
                    currentInterventions.RemoveAt(index);
                    interventionViewSource.Source = currentInterventions;
                    IsChanged = false;
                    SetButtons();
                }

            }
        }

        private void RemoveEmptyCustomerGate()
        {
            if (customerGateDataGrid.SelectedIndex > -1)
            {
                CustomerGate gate = (CustomerGate)customerGateDataGrid.SelectedItem;
                if (gate.Id == 0)
                {
                    int index = customerGateDataGrid.SelectedIndex;
                    currentGates.RemoveAt(index);
                    customerGateViewSource.Source = currentGates;
                    IsChanged = false;
                    SetButtons();
                }
            }
        }

        private void RemoveEmptyCustomer()
        {
            if (customerDataGrid.SelectedIndex > -1)
            {
                Customer customer = (Customer)customerDataGrid.SelectedItem;
                if (customer.Id == 0)
                {
                    int index = customerDataGrid.SelectedIndex;
                    currentCustomers.RemoveAt(index);
                    customerViewSource.Source = currentCustomers;
                    IsChanged = false;
                    SetButtons();
                }
            }
        }

        private void contextColorMenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            ColorPickerWindow CP = new ColorPickerWindow();
            if (CP.ShowDialog() == true)
            {
                
            }
        }
    }
}
