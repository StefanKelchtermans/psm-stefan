﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for LegalFormManagementWindow.xaml
    /// </summary>
    public partial class LegalFormManagementWindow : Window
    {
        private System.Windows.Data.CollectionViewSource legalCapacityViewSource;
        private PSMUser currentUser;
        private ObservableCollection<LegalCapacity> legalcapacities;

        public LegalFormManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            legalCapacityViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("legalCapacityViewSource")));
            ReLoadLegalCapacities();
        }

        private void ReLoadLegalCapacities()
        {
            LegalCapacity_services lService = new LegalCapacity_services();
            legalcapacities = lService.GetAllObservable();
            legalCapacityViewSource.Source = legalcapacities;
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (legalCapacityDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de wettelijke hoedanigheid verwijderen?", "Verwijderen wettelijke hoedanigheid", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    LegalCapacity legal = (LegalCapacity)legalCapacityDataGrid.SelectedItem;
                    LegalCapacity_services lService = new LegalCapacity_services();
                    legal.Modified = DateTime.Now;
                    legal.ModifiedBy = currentUser.FirstName;
                    lService.Delete(legal);
                    ReLoadLegalCapacities();
                    MessageBox.Show("Wettelijke hoedanigheid verwijderd.", "Verwijderen wettelijke hoedanigheid");
                }
            }

        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<LegalCapacity>(from m in legalcapacities
                                                                   where m.Name.ToLower().StartsWith(searchString.ToLower())
                                                                   orderby m.Name
                                                                   select m).ToList();

                legalCapacityViewSource.Source = gevonden;
            }
            else
                legalCapacityViewSource.Source = legalcapacities;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (legalCapacityDataGrid.SelectedIndex > -1)
            {
                LegalCapacity capacity = (LegalCapacity)legalCapacityDataGrid.SelectedItem;
                if (capacity.Id == 0)
                {
                    int i = 0;
                    foreach (LegalCapacity item in legalcapacities)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    legalcapacities.RemoveAt(i);
                    legalCapacityViewSource.Source = null;
                    legalCapacityViewSource.Source = legalcapacities;
                }
            }
        }


        private void NewElement()
        {
            LegalCapacity newlegal = new LegalCapacity();
            newlegal.Id = 0;
            legalcapacities.Add(newlegal);
            legalCapacityViewSource.Source = null;
            legalCapacityViewSource.Source = legalcapacities;
            legalCapacityDataGrid.SelectedIndex = legalcapacities.Count - 1;
            legalCapacityDataGrid.ScrollIntoView(newlegal);
        }

        private void SaveData()
        {
            if (legalCapacityDataGrid.SelectedIndex > -1)
            {
                LegalCapacity legal = (LegalCapacity)legalCapacityDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateLegalCapacity(legal);
                if (valid.IsValid)
                {
                    LegalCapacity_services lService = new LegalCapacity_services();
                    if (legal.Id == 0)
                    {
                        legal.Active = true;
                        legal.Created = DateTime.Now;
                        legal.CreatedBy = currentUser.FirstName;
                        legal.Modified = DateTime.Now;
                        legal.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            LegalCapacity saved = lService.Save(legal);
                            ReLoadLegalCapacities();
                            MessageBox.Show("Wettelijke hoedanigheid opgeslagen.", "Opslaan Wettelijke hoedanigheid");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        legal.Modified = DateTime.Now;
                        legal.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            lService.Update(legal);
                            ReLoadLegalCapacities();
                            MessageBox.Show("Wettelijke hoedanigheid opgeslagen.", "Opslaan Wettelijke hoedanigheid");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
