﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for GateDecorations.xaml
    /// </summary>
    public partial class GateDecorations : Window
    {
        private System.Windows.Data.CollectionViewSource gateDecorationViewSource;
        private ObservableCollection<GateDecoration> gatedecorations;
        private PSMUser currentUser;

        public GateDecorations(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void ReloadDecorations()
        {
            Gate_Decoration_services gdService = new Gate_Decoration_services();
            gatedecorations = gdService.GetAllObservable();
            gateDecorationViewSource.Source = gatedecorations;
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (gateDecorationDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u het beslag verwijderen?", "Verwijderen beslag", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    GateDecoration decoration = (GateDecoration)gateDecorationDataGrid.SelectedItem;
                    Gate_Decoration_services dService = new Gate_Decoration_services();
                    decoration.Modified = DateTime.Now;
                    decoration.ModifiedBy = currentUser.FirstName;
                    dService.Delete(decoration);
                    ReloadDecorations();
                    MessageBox.Show("Beslag verwijderd.", "Verwijderen beslag");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            gateDecorationViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("gateDecorationViewSource")));
            ReloadDecorations();
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<GateDecoration>(from d in gatedecorations
                                                                    where d.Name.ToLower().StartsWith(searchString.ToLower())
                                                                    orderby d.Name
                                                                    select d).ToList();

                gateDecorationViewSource.Source = gevonden;
            }
            else
                gateDecorationViewSource.Source = gatedecorations;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (gateDecorationDataGrid.SelectedIndex > -1)
            {
                GateDecoration gatedeco = (GateDecoration)gateDecorationDataGrid.SelectedItem;
                if (gatedeco.Id == 0)
                {
                    int i = 0;
                    foreach (GateDecoration item in gatedecorations)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    gatedecorations.RemoveAt(i);
                    gateDecorationViewSource.Source = null;
                    gateDecorationViewSource.Source = gatedecorations;
                }
            }
        }

        private void NewElement()
        {
            GateDecoration newDecoration = new GateDecoration();
            newDecoration.Id = 0;
            gatedecorations.Add(newDecoration);
            gateDecorationViewSource.Source = null;
            gateDecorationViewSource.Source = gatedecorations;
            gateDecorationDataGrid.SelectedIndex = gatedecorations.Count - 1;
            gateDecorationDataGrid.ScrollIntoView(newDecoration);
        }

        private void SaveData()
        {
            if (gateDecorationDataGrid.SelectedIndex > -1)
            {
                GateDecoration decoration = (GateDecoration)gateDecorationDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateGateDecoration(decoration);
                if (valid.IsValid)
                {
                    Gate_Decoration_services dService = new Gate_Decoration_services();
                    if (decoration.Id == 0)
                    {
                        decoration.Active = true;
                        decoration.Created = DateTime.Now;
                        decoration.CreatedBy = currentUser.FirstName;
                        decoration.Modified = DateTime.Now;
                        decoration.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            GateDecoration saved = dService.Save(decoration);
                            ReloadDecorations();
                            MessageBox.Show("Beslag opgeslagen.", "Opslaan beslag");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        decoration.Modified = DateTime.Now;
                        decoration.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            dService.Update(decoration);
                            ReloadDecorations();
                            MessageBox.Show("Beslag opgeslagen.", "Opslaan beslag");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
