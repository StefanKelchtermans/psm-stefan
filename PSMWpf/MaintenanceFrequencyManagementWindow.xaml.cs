﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for MaintenanceFrequencyManagementWindow.xaml
    /// </summary>
    public partial class MaintenanceFrequencyManagementWindow : Window
    {
        private System.Windows.Data.CollectionViewSource maintenanceFrequencyViewSource;
        private PSMUser currentUser;
        private ObservableCollection<MaintenanceFrequency> maintenanceFreqencies;

        public MaintenanceFrequencyManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<MaintenanceFrequency>(from f in maintenanceFreqencies
                                                                   where f.Frequency.ToString() == searchString
                                                                   select f).ToList();

                maintenanceFrequencyViewSource.Source = gevonden;
            }
            else
                maintenanceFrequencyViewSource.Source = maintenanceFreqencies;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();

        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (maintenanceFrequencyDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u het poortmodel verwijderen?", "Verwijderen poortmodel", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    MaintenanceFrequency frequentie = (MaintenanceFrequency)maintenanceFrequencyDataGrid.SelectedItem;
                    Maintenance_frequency_services mService = new Maintenance_frequency_services();
                    frequentie.Modified = DateTime.Now;
                    frequentie.ModifiedBy = currentUser.FirstName;
                    mService.Delete(frequentie);
                    ReloadFrequencies();
                    MessageBox.Show("Sturing poortmodel.", "Verwijderen poortmodel");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            maintenanceFrequencyViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("maintenanceFrequencyViewSource")));
            ReloadFrequencies();
        }

        private void ReloadFrequencies()
        {
            Maintenance_frequency_services mService = new Maintenance_frequency_services();
            maintenanceFreqencies = mService.GetAllObservable();
            maintenanceFrequencyViewSource.Source = maintenanceFreqencies;
        }

        private void frequencyTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            if (!string.IsNullOrEmpty(box.Text))
            {
                if (OurValidation.IsNaN(box.Text))
                {
                    MessageBox.Show("De ingegeven waarde is geen geheel getal!");
                    SaveButton.IsEnabled = false;
                }
                else
                    SaveButton.IsEnabled = true;
            }
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (maintenanceFrequencyDataGrid.SelectedIndex > -1)
            {
                MaintenanceFrequency frequency = (MaintenanceFrequency)maintenanceFrequencyDataGrid.SelectedItem;
                if (frequency.Id == 0)
                {
                    int i = 0;
                    foreach (MaintenanceFrequency item in maintenanceFreqencies)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    maintenanceFreqencies.RemoveAt(i);
                    maintenanceFrequencyViewSource.Source = null;
                    maintenanceFrequencyViewSource.Source = maintenanceFreqencies;
                }
            }
        }


        private void NewElement()
        {
            MaintenanceFrequency newFrequency = new MaintenanceFrequency();
            newFrequency.Id = 0;
            maintenanceFreqencies.Add(newFrequency);
            maintenanceFrequencyViewSource.Source = null;
            maintenanceFrequencyViewSource.Source = maintenanceFreqencies;
            maintenanceFrequencyDataGrid.SelectedIndex = maintenanceFreqencies.Count - 1;
            maintenanceFrequencyDataGrid.ScrollIntoView(newFrequency);
        }

        private void SaveData()
        {
            if (maintenanceFrequencyDataGrid.SelectedIndex > -1)
            {
                MaintenanceFrequency frequency = (MaintenanceFrequency)maintenanceFrequencyDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateMaintenanceFrequency(frequency);
                if (valid.IsValid)
                {
                    Maintenance_frequency_services mService = new Maintenance_frequency_services();
                    if (frequency.Id == 0)
                    {
                        frequency.Active = true;
                        frequency.Created = DateTime.Now;
                        frequency.CreatedBy = currentUser.FirstName;
                        frequency.Modified = DateTime.Now;
                        frequency.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            MaintenanceFrequency saved = mService.Save(frequency);
                            ReloadFrequencies();
                            MessageBox.Show("Onderhoudsfrequentie opgeslagen.", "Opslaan Onderhoudsfrequentie");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        frequency.Modified = DateTime.Now;
                        frequency.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            mService.Update(frequency);
                            ReloadFrequencies();
                            MessageBox.Show("Onderhoudsfrequentie opgeslagen.", "Opslaan Onderhoudsfrequentie");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
