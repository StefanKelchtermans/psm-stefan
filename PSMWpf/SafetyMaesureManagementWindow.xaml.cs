﻿using PSMClassLibrary;
using PSMData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for SafetyMaesureManagementWindow.xaml
    /// </summary>
    public partial class SafetyMaesureManagementWindow : Window
    {
        private PSMUser currentUser;
        private System.Windows.Data.CollectionViewSource safetyMeasureViewSource;
        private ObservableCollection<SafetyMeasure> safetyMeasures;
        
        public SafetyMaesureManagementWindow(PSMUser user)
        {
            InitializeComponent();
            currentUser = user;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox box = (TextBox)sender;
            string searchString = box.Text;
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var gevonden = new ObservableCollection<SafetyMeasure>((from s in safetyMeasures
                                                                        where s.Name.ToLower().StartsWith(searchString.ToLower())
                                                                        select s).ToList());

                safetyMeasureViewSource.Source = gevonden;
            }
            else
                safetyMeasureViewSource.Source = safetyMeasures;

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            NewElement();

        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (safetyMeasureDataGrid.SelectedIndex > -1)
            {
                if (MessageBox.Show("Wil u de Veiligheidsmaatregeling verwijderen?", "Verwijderen Veiligheidsmaatregeling", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    SafetyMeasure safety = (SafetyMeasure)safetyMeasureDataGrid.SelectedItem;
                    SafetyMeasure_services sService = new SafetyMeasure_services();
                    safety.Modified = DateTime.Now;
                    safety.ModifiedBy = currentUser.FirstName;
                    sService.Delete(safety);
                    ReloadSafetyMeasures();
                    MessageBox.Show("Veiligheidsmaatregeling verwijderd.", "Verwijderen Veiligheidsmaatregeling");
                }
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            safetyMeasureViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("safetyMeasureViewSource")));
            ReloadSafetyMeasures();
        }

        private void ReloadSafetyMeasures()
        {
            SafetyMeasure_services sService = new SafetyMeasure_services();
            safetyMeasures = sService.GetAllObservable();
            safetyMeasureViewSource.Source = safetyMeasures;
        }

        private void Label_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (SaveButton.IsEnabled == true)
                    SaveData();
            }
            if (e.Key == Key.N && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                NewElement();
            }
            if (e.Key == Key.F4)
            {
                DialogResult = true;
            }
            if (e.Key == Key.Escape)
            {
                RemoveEmptyElement();
            }
        }

        private void RemoveEmptyElement()
        {
            if (safetyMeasureDataGrid.SelectedIndex > -1)
            {
                SafetyMeasure safety = (SafetyMeasure)safetyMeasureDataGrid.SelectedItem;
                if (safety.Id == 0)
                {
                    int i = 0;
                    foreach (SafetyMeasure item in safetyMeasures)
                    {
                        if (item.Id == 0)
                            break;
                        else
                            i++;
                    }
                    safetyMeasures.RemoveAt(i);
                    safetyMeasureViewSource.Source = null;
                    safetyMeasureViewSource.Source = safetyMeasures;
                }
            }
        }


        private void NewElement()
        {
            SafetyMeasure newSafety = new SafetyMeasure();
            newSafety.Id = 0;
            safetyMeasures.Add(newSafety);
            safetyMeasureViewSource.Source = null;
            safetyMeasureViewSource.Source = safetyMeasures;
            safetyMeasureDataGrid.SelectedIndex = safetyMeasures.Count - 1;
            safetyMeasureDataGrid.ScrollIntoView(newSafety);
        }

        private void SaveData()
        {
            if (safetyMeasureDataGrid.SelectedIndex > -1)
            {
                SafetyMeasure safety = (SafetyMeasure)safetyMeasureDataGrid.SelectedItem;
                Valid valid = OurValidation.ValidateSafetyMaesure(safety);
                if (valid.IsValid)
                {
                    SafetyMeasure_services sService = new SafetyMeasure_services();
                    if (safety.Id == 0)
                    {
                        safety.Active = true;
                        safety.Created = DateTime.Now;
                        safety.CreatedBy = currentUser.FirstName;
                        safety.Modified = DateTime.Now;
                        safety.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            SafetyMeasure saved = sService.Save(safety);
                            ReloadSafetyMeasures();
                            MessageBox.Show("Veiligheidsmaatregeling opgeslagen.", "Opslaan Veiligheidsmaatregeling");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        safety.Modified = DateTime.Now;
                        safety.ModifiedBy = currentUser.FirstName;
                        try
                        {
                            sService.Update(safety);
                            ReloadSafetyMeasures();
                            MessageBox.Show("Veiligheidsmaatregeling opgeslagen.", "Opslaan Veiligheidsmaatregeling");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                    MessageBox.Show(valid.Message);
            }
        }
    }
}
