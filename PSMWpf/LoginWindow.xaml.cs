﻿using PSMData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PSMWpf
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            TryLogin();
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TryLogin();
            }
            else
            {
                this.Height = 372;
            }
        }

        private void TryLogin()
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Password;
            PSMUser_service uService = new PSMUser_service();
            PSMUser user = uService.GetUserByNameAndPassword(username, password);
            if (user == null)
            {
                errorMessageLabel.Content = "Gebruikersnaam of paswoord is niet correct! Probeer opnieuw.";
                this.Height = 400;
            }
            else
            {
                MainWindow MW = new MainWindow(user);
                MW.Show();
                this.Hide();
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }
    }
}
