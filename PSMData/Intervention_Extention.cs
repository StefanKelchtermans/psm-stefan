﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public partial class Intervention
    {
        public string WaitForFeedBackNL 
        {
            get
            {
                return WaitForFeedBack == true ? "Ja": "Nee";
            }
        }
    }
}
