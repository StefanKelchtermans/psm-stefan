﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Service_Installation_services
    {
        public List<ServiceInstallation> GetAll()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return (from sm in entities.ServiceInstallations
                        where sm.Active == true
                        orderby sm.Name
                        select sm).ToList();
            }
        }

        public ServiceInstallation GetByName(string name)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return (from s in entities.ServiceInstallations
                        where s.Name == name
                        select s).FirstOrDefault();
            }
        }

        public ObservableCollection<ServiceInstallation> GetAllObservable()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<ServiceInstallation>((from sm in entities.ServiceInstallations
                                                                      where sm.Active == true
                                                                      orderby sm.Name
                                                                      select sm).ToList());
            }
        }

        public ServiceInstallation Save(ServiceInstallation installation)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from s in entities.ServiceInstallations
                        where s.Name.ToLower() == installation.Name.ToLower()
                        select s).FirstOrDefault();

                if (found == null)
                {
                    entities.ServiceInstallations.Add(installation);
                    entities.SaveChanges();
                    return installation;
                }
                else
                {
                    throw new Exception("De bedieningsinstallatie bestaat reeds!");
                }
            }
        }

        public void Update(ServiceInstallation installation)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from s in entities.ServiceInstallations
                             where s.Id == installation.Id
                             select s).FirstOrDefault();

                if (found != null)
                {
                    found.Name = installation.Name;
                    found.Active = installation.Active;
                    found.Modified = installation.Modified;
                    found.ModifiedBy = installation.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De bedieningsinstallatie is niet gevonden!");
                }
            }
        }

        public void Delete(ServiceInstallation installation)
        {
            installation.Active = false;
            Update(installation);
        }
    }
}
