﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Gate_Decoration_services
    {
        public List<GateDecoration> GetAll()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return (from gd in entities.GateDecorations
                        where gd.Active == true
                        orderby gd.Name
                        select gd).ToList();
            }
        }

        public ObservableCollection<GateDecoration> GetAllObservable()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<GateDecoration>((from gd in entities.GateDecorations
                                                                 where gd.Active == true
                                                                 orderby gd.Name
                                                                 select gd).ToList());
            }
        }

        public GateDecoration Save(GateDecoration decoration)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from d in entities.GateDecorations
                             where d.Name == decoration.Name
                             select d).FirstOrDefault();

                if (found == null)
                {
                    entities.GateDecorations.Add(decoration);
                    entities.SaveChanges();
                    return decoration;
                }
                else
                    throw new Exception("Dit beslag staat reeds in de lijst!!");
            }
        }

        public void Update(GateDecoration decoration)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from d in entities.GateDecorations
                             where d.Id == decoration.Id
                             select d).FirstOrDefault();

                if (found != null)
                {
                    found.Active = decoration.Active;
                    found.Name = decoration.Name;
                    found.Modified = decoration.Modified;
                    found.ModifiedBy = decoration.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                    throw new Exception("Het beslag is niet gevonden!!");
            }
        }

        public void Delete(GateDecoration decoration)
        {
            decoration.Active = false;
            Update(decoration);
        }

    }
}
