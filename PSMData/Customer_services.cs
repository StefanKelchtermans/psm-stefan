﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Customer_services
    {
        public ObservableCollection<Customer> GetAll() 
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<Customer>((from c in entities.Customers.Include("City1").Include("City1.Country1").Include("LegalCapacity1").Include("Language1")
                        where c.Active == true
                        orderby c.Name
                        select c).ToList());
            }
        }


        public ObservableCollection<Customer> GetBySearchTerms(CustomerSearchTerms searchterms)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var query = from c in entities.Customers.Include("City1").Include("City1.Country1").Include("LegalCapacity1").Include("Language1")
                            select c;
                query = query.AsQueryable().Where(c => c.Active == true);

                if (searchterms.Code != "" && searchterms.Code != null)
                    query = query.AsQueryable().Where(c => c.Code == searchterms.Code);

                if (searchterms.Name != "" && searchterms.Name != null)
                    query = query.AsQueryable().Where(c => c.Name.ToLower().StartsWith(searchterms.Name.ToLower()));

                if (searchterms.Address != "" && searchterms.Address != null)
                    query = query.AsQueryable().Where(c => c.Address.ToLower().Contains(searchterms.Address.ToLower()));

                if (searchterms.City != null)
                    query = query.AsQueryable().Where(c => c.City == searchterms.City.Id);


                query = query.AsQueryable().OrderBy(c => c.Name);

                var customers = query.ToList();

                if ((searchterms.GateColor == null || searchterms.GateColor == "") && searchterms.GateModel == null && searchterms.InstalationAfter == null)
                    return new ObservableCollection<Customer>(customers);

                // Customergate filter options : GateColor, GateModel, Installationdate
                var query2 = from g in entities.CustomerGates.Include("Customer1").Include("Customer1.City1").Include("Customer1.City1.Country1").Include("Customer1.LegalCapacity1").Include("Customer1.Language1")
                             select g;

                query2 = query2.AsQueryable().Where(g => g.Active == true);

                if(searchterms.GateColor != "")
                    query2 = query2.AsQueryable().Where(g => g.GateColor.ToLower() == searchterms.GateColor.ToLower());

                if(searchterms.GateModel != null)
                    query2 = query2.AsQueryable().Where(g => g.GateModel1.Id == searchterms.GateModel.Id);

                if (searchterms.InstalationAfter != null)
                    query2 = query2.AsQueryable().Where(g => g.InstallationDate > searchterms.InstalationAfter);

                var gates = query2.ToList();

                // Merge the List

                List<Customer> Filtered = new List<Customer>();

                foreach (CustomerGate gate in gates)
                {
                    foreach (Customer customer in customers)
                    {
                        if (customer.Id == gate.Customer1.Id)
                        {
                            if (!Filtered.Contains(gate.Customer1))
                                Filtered.Add(gate.Customer1);
                            break;
                        }
                    }
                }
                return new ObservableCollection<Customer>(Filtered.OrderBy(c => c.Name));
            }
        }

        public Customer Save(Customer customer)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var gevonden = (from c in entities.Customers
                                where c.Name.ToLower().Equals(customer.Name.ToLower())
                                && c.City == customer.City
                                select c).FirstOrDefault();

                if (gevonden == null)
                {
                    customer.City1 = null;
                    customer.LegalCapacity1 = null;
                    customer.Language1 = null;
                    entities.Customers.Add(customer);
                    entities.SaveChanges();
                    return (from c in entities.Customers.Include("City1").Include("City1.Country1").Include("LegalCapacity1").Include("Language1")
                            where c.Id == customer.Id
                            select c).FirstOrDefault();
                }
                else
                    throw new Exception("De klant bestaat reeds!");


            }
        }

        public void Update(Customer customer)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from c in entities.Customers
                                where c.Id == customer.Id
                                select c).FirstOrDefault();

                if (found != null)
                {
                    found.Name = customer.Name;
                    found.Code = customer.Code;
                    found.City = customer.City;
                    found.LegalCapacity = customer.LegalCapacity;
                    found.Language = customer.Language;
                    found.Phone = customer.Phone;
                    found.MobilePhone = customer.MobilePhone;
                    found.Email = customer.Email;
                    found.VatNumber = customer.VatNumber;
                    found.Address = customer.Address;
                    found.Modified = customer.Modified;
                    found.ModifiedBy = customer.ModifiedBy;

                    entities.SaveChanges();
                }
                else
                    throw new Exception("Klant niet gevonden!");


            }
        }

        public void Delete(Customer customer)
        {
            customer.Active = false;
            Update(customer);
        }
    }
}
