﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class GateDriver_services
    {
        public List<GateDriver> GetAll()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return (from d in entities.GateDrivers
                        where d.Active == true
                        orderby d.Name
                        select d).ToList();
            }
        }

        public ObservableCollection<GateDriver> GetAllObservable()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<GateDriver>((from d in entities.GateDrivers
                                                             where d.Active == true
                                                             orderby d.Name
                                                             select d).ToList());
            }
        }

        public void Delete(GateDriver driver)
        {
            driver.Active = false;
            Update(driver);
        }

        public GateDriver Save(GateDriver driver)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from d in entities.GateDrivers
                             where d.Name.ToLower() == driver.Name.ToLower()
                             select d).FirstOrDefault();
                if (found == null)
                {
                    entities.GateDrivers.Add(driver);
                    entities.SaveChanges();

                    return driver;
                }
                else
                    throw new Exception("Deze sturing staat reeds in de lijst!");
            }
        }

        public void Update(GateDriver driver)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from d in entities.GateDrivers
                             where d.Id == driver.Id
                             select d).FirstOrDefault();

                if (found != null)
                {
                    found.Name = driver.Name;
                    found.Active = driver.Active;
                    found.Modified = driver.Modified;
                    found.ModifiedBy = driver.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                    throw new Exception("De sturing is niet gevonden!");
            }
        }

    }
}
