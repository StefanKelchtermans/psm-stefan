﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Intervention_services
    {
        public ObservableCollection<Intervention> GetByCustomerGate(int customergate_Id)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<Intervention>((from i in entities.Interventions.Include("InterventionType1")
                                                               where i.Active == true
                                                               && i.CustomerGate == customergate_Id
                                                               select i).ToList());
            }
        }

        public Intervention Save(Intervention intervention)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from i in entities.Interventions
                             where i.InterventionType == intervention.InterventionType
                             && i.PlannedDate == intervention.PlannedDate
                             && i.CustomerGate == intervention.CustomerGate
                             select i).FirstOrDefault();

                if (found == null)
                {
                    entities.Interventions.Add(intervention);
                    entities.SaveChanges();

                    return (from i in entities.Interventions.Include("InterventionType1")
                            where i.Id == intervention.Id
                            select i).FirstOrDefault();
                }
                else
                    throw new Exception("De interventie is reeds geregistreerd voor deze datum!");
            }
        }

        public Intervention Update(Intervention intervention)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from i in entities.Interventions
                             where i.Id == intervention.Id
                             select i).FirstOrDefault();

                if (found != null)
                {
                    found.InterventionType = intervention.InterventionType;
                    found.Description = intervention.Description;
                    found.ExecutionDate = intervention.ExecutionDate;
                    found.PlannedDate = intervention.PlannedDate;
                    found.Remarks = intervention.Remarks;
                    found.WaitForFeedBack = intervention.WaitForFeedBack;
                    found.Active = intervention.Active;
                    found.Modified = intervention.Modified;
                    found.ModifiedBy = intervention.ModifiedBy;
                    entities.SaveChanges();

                    return intervention;
                }
                else
                    throw new Exception("Geen overeenkomstige interventie gevonden!");
            }
        }

        public void Delete(Intervention intervention)
        {
            intervention.Active = false;
            Update(intervention);
        }

        public ObservableCollection<Intervention> GetByPlannedDate(DateTime searchdate)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<Intervention>((from i in entities.Interventions.Include("CustomerGate1").Include("CustomerGate1.GateModel1").Include("CustomerGate1.Customer1").Include("CustomerGate1.Customer1.City1")
                                                               where i.Active == true
                                                               && i.PlannedDate < searchdate
                                                               orderby i.PlannedDate descending
                                                               select i).ToList());
            }
        }
    }
}
