﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class SafetyMeasure_services
    {
        public List<SafetyMeasure> GetAll()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return (from sm in entities.SafetyMeasures
                        where sm.Active == true
                        orderby sm.Name
                        select sm).ToList();
            }
        }

        public SafetyMeasure GetByName(string name)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return (from sm in entities.SafetyMeasures
                        where sm.Name == name
                        select sm).FirstOrDefault();
            }
        }

        public ObservableCollection<SafetyMeasure> GetAllObservable()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<SafetyMeasure>((from sm in entities.SafetyMeasures
                        where sm.Active == true
                        orderby sm.Name
                        select sm).ToList());
            }
        }

        public SafetyMeasure Save(SafetyMeasure safety)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from sm in entities.SafetyMeasures
                        where sm.Name.ToLower() == safety.Name.ToLower()
                        select sm).FirstOrDefault();

                if (found == null)
                {
                    entities.SafetyMeasures.Add(safety);
                    entities.SaveChanges();

                    return safety;
                }
                else
                {
                    throw new Exception("De veiligheidsmaatregel bestaat reeds!");
                }
            }
        }

        public void Update(SafetyMeasure safety)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from sm in entities.SafetyMeasures
                             where sm.Id == safety.Id
                             select sm).FirstOrDefault();

                if (found != null)
                {
                    found.Name = safety.Name;
                    found.Active = safety.Active;
                    found.Modified = safety.Modified;
                    found.ModifiedBy = safety.ModifiedBy;
                    entities.SaveChanges();
                }
                else
                {
                    throw new Exception("De veiligheidsmaatregel is niet gevonden!");
                }
            }
        }

        public void Delete(SafetyMeasure safety)
        {
            safety.Active = false;
            Update(safety);
        }
    }
}
