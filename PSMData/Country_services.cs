﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSMData
{
    public class Country_services
    {
        public List<Country> GetAll()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return (from c in entities.Countries
                        where c.Active == true
                        orderby c.Name
                        select c).ToList();
            }
        }

        public ObservableCollection<Country> GetAllObservable()
        {
            using (var entities = new PoortserviceTestEntities())
            {
                return new ObservableCollection<Country>((from c in entities.Countries
                                                          where c.Active == true
                                                          orderby c.Name
                                                          select c).ToList());
            }
        }

        public Country Save(Country country)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from c in entities.Countries
                             where c.Name.ToLower().Equals(country.Name.ToLower())
                             select c).FirstOrDefault();

                if (found == null)
                {
                    entities.Countries.Add(country);
                    entities.SaveChanges();
                }
                else
                    throw new Exception("Er is al een land met deze naam!");
                return country;
            }
        }

        public void Update(Country country)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                var found = (from c in entities.Countries
                             where c.Id == country.Id
                             select c).FirstOrDefault();

                if (found != null)
                {
                    found.Name = country.Name;
                    found.Code = country.Code;
                    found.Active = country.Active;
                    found.Modified = country.Modified;
                    found.ModifiedBy = country.ModifiedBy;
                    entities.SaveChanges();
                }
            }
        }

        public void Delete(Country country)
        {
            using (var entities = new PoortserviceTestEntities())
            {
                country.Active = false;
                Update(country);
            }
        }
    }
}
